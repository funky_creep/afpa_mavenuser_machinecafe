package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import model.Model;

public class Controller implements ActionListener {

	private Model model;
	
	public Controller(Model m) {
		this.model = m;
	}

	public void actionPerformed(ActionEvent e) {
		//System.out.println("Ici le Controller.");
		model.setChangedNotifyObservers();
	}
	
	public void init() {
		model.setChangedNotifyObservers();
	}
	
}