package com.afpasigfox.machinecafedao.UserMachineCafe;

import controller.Controller;
import model.Model;
import view.View;

public class Main {

	public static void main(String[] args) {
		
		Model myModel = new Model();
		Controller myController = new Controller(myModel);
		
		View view = new View();
		view.setListener(myController);
		myModel.addObserver(view);
		
		myController.init();
		view.setVisible(true);

	}

}