package view;

import java.awt.BorderLayout;
import java.awt.Container;
//import java.awt.FlowLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;

import model.Model;

@SuppressWarnings("deprecation")
public class View extends JFrame implements Observer {
	
	//private Container myFirstContainer = getContentPane();
	private JButton button = new JButton("Click here");
	
	// Containers
	private Container mainContainer = getContentPane();
	private JPanel centerPanel = new JPanel(new FlowLayout());
	private JPanel leftPanel = new JPanel(new GridLayout(0,2));
	private JPanel rightPanel = new JPanel(new GridLayout(0,2));
	
	// Liste de boutons
	private ArrayList<JButton> buttons = new ArrayList<JButton>();
	
	// Titre
	private JLabel title = new JLabel("Distributeur de boissons");
	
	// Zone de texte
	private JTextArea textArea = new JTextArea("Texte");
	
	public View() {
		setSize(400,200);
		//mainContainer.setLayout(new FlowLayout());
		//mainContainer.add(button);
		
		//createDrinkButtons();
		//createCoinsButtons();
		
		mainContainer.setLayout(new BorderLayout());
		mainContainer.add(title, BorderLayout.NORTH);
		this.setTitle("Distributeur de boissons");
		
		centerPanel.add(leftPanel);
		centerPanel.add(rightPanel);
		mainContainer.add(centerPanel);
		
		mainContainer.add(textArea, BorderLayout.SOUTH);
	}
	
	public void setListener(ActionListener al) {
		button.addActionListener(al);
	}

	public void update(Observable o, Object arg) {
		System.out.println(((Model) o).returnFirst());
		
		
	}
	
}