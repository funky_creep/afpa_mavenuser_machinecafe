package model;

import java.util.Observable;

import exceptions.ExceptionRequest;
import model.beans.Produit;
import model.utils.Connexion;

public class Model extends Observable {

	private Stockeur<Produit> stockProduits;
	
	public Model() {
		DAOFactory f = new DAOFactory(new Connexion("jdbc:postgresql://localhost:5432/db_machinecafe", "postgres", "postgres", "org.postgresql.Driver"));
		try {
			stockProduits = f.getDAO(DAOFactory.DAOBoisson).findAll();
		} catch (ExceptionRequest e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void setChangedNotifyObservers() {
		setChanged();
		notifyObservers();
	}
	
	public String returnFirst() {
		return stockProduits.getItem(1).getNom();
	}
}
